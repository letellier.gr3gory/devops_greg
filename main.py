from duck import Duck
from client import Client


if __name__ == "__main__":
    print("starting ... ")

    duckA = Duck("duckA", "male", 12.0)
    duckA.greet()

    clientA = Client('clientA', 15.0)
    clientB = Client('clientB', 10.0)

    clientA.buy(duckA)
    clientB.buy(duckA)
