class Client:

    def __init__(self, name, amount):
        self.name = name
        self.amount = amount
        self.duck = None

    def buy(self, duck):
        if self.amount > duck.price :
            self.amount -= duck.price
            self.duck = duck
        else:
            print("You need more money to buy this duck  ! ")
            pass
